<?php

require_once DOL_DOCUMENT_ROOT.'/core/class/commonobject.class.php';

class Tool extends CommonObject{
	public $element = "tools";



	var $libelle;


	/**
	 * Tool label
	 * @var string
	 */
	var $label;


    /**
     * Tool descripion
     * @var string
     */
	var $description;
	/**
     * Tool's zone
     * @var string
     */
	var $zone;


}
	